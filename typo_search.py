# Copyright (C) 2016-2017 Nick Spinale <spinalen@gmail.com>

import re
import sys
import json
from argparse import ArgumentParser
from urllib.request import urlopen
from urllib.parse import urlencode

from whois import whois
from whois.parser import PywhoisError


def get_typos(words):
    TYPO_URL = 'http://tools.seobook.com/spelling/keywords-typos.cgi'
    query = [
            ('user_input', '\x0D\x0A'.join(words)),
            ('skip_letter', 'skip_letter'),
            ('double_letters', 'double_letters'),
            ('reverse_letters', 'reverse_letters'),
            ('skip_spaces', 'skip_spaces'),
            ('missed_key', 'missed_key'),
            ('inserted_key', 'inserted_key'),
        ]
    data = urlencode(query).encode()
    resp = urlopen(TYPO_URL, data)
    body = resp.read().decode('utf-8')
    r = re.compile('<textarea r[^>]*>([^<]*)<', flags=re.MULTILINE)
    m = r.search(body)
    typos = m.group(1).splitlines()
    return typos


# reconstructed from instantdomainsearch.com's minified js:
# a(e){for(var t=arguments.length>1&&void 0!==arguments[1]?arguments[1]:0,n=e.length,r=0;r<n;++r)t=(t<<5)-t+e.charCodeAt(r),t&=t;return t}
def hash_code(e, c):
    t = c
    for b in e.encode('ascii'):
        t = (t << 5) - t + b
        t &= t
    return t


def check_domain(predot, postdot):
    DOMAIN_URL_BASE = 'https://instantdomainsearch.com/services/all/'
    hsh = hash_code(predot, 42) # wut
    query = [
            ('tldTags', 'popular'),
            ('partTld', ''),
            ('country', 'HU'),
            ('city', 'budapest'),
            ('hash', hsh),
        ]
    url = DOMAIN_URL_BASE + predot + '?' + urlencode(query)
    resp = urlopen(url)
    for line in resp:
        data = json.loads(line.decode('utf-8'))
        if 'label' in data and data['label'] == predot and data['tld'] == postdot:
            if data['isRegistered']:
                return False
            else:
                try:
                    whois(predot + '.' + postdot)
                    return False
                except PywhoisError:
                    return True
    raise Exception('instantdomainsearch.com really isn\'t the best solution here')


def check_typos(predot, postdot):
    for typo in get_typos([predot]):
        if check_domain(typo, postdot):
            yield typo


def main():
    parser = ArgumentParser(description='Find available typo domains.')
    parser.add_argument('domains', metavar='DOMAIN', nargs='+', help='Target domains')
    args = parser.parse_args()
    pairs = []
    for domain in args.domains:
        pair = domain.split('.')
        if len(pair) == 2:
            pairs.append(pair)
        else:
            sys.exit('***error*** ' + domain + ' is not a valid domain name.')
    for predot, postdot in pairs:
        for typo in check_typos(predot, postdot):
            print(typo + '.' + postdot)


if __name__ == '__main__':
    main()

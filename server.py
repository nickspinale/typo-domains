import typo_search
from flask import Flask


app = Flask(__name__)


@app.route('/check/<domain>')
def go(domain):
    try:
        predot, postdot = domain.split('.')
        print(postdot)
    except:
        return '***error*** ' + domain + ' is not a valid domain name.'
    good = []
    try:
        for typo in typo_search.check_typos(predot, postdot):
            good.append(typo + '.' + postdot)
    except Exception as e:
        good.append('***error*** too many requests to instantdomainsearch.com')
    return ''.join([
            '<html><header><title>Typo Domain Finder</title></header>',
            '<body>',
            '<h2>Available typos of ' + predot + '.' + postdot + '</h2>',
            '<ul>',
            ''.join(map(lambda x: '<li>' + x + '</li>', good)),
            '</ul>',
            '</body>',
            '</html>'
        ])


if __name__ == '__main__':
    app.run()
